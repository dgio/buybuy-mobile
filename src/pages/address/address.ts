import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Camera } from "@ionic-native/camera";
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms'
import { RegisterService } from '../../services/register-service';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'page-address',
    templateUrl: 'address.html',
})

export class Address {
    public imageData: string;
    public verify: boolean = false;
    public verified: boolean = false;

    public colony: any = "";
    public street: any = "";
    public number: any = "";
    public zipCode: any = "";
    public details: any = "";

    public isSent: boolean = false;
    public form: FormGroup;

    public userId;
    public userNew: any;

    public latitude: any;
    public longitude: any;
    public usuario = {
        suburb: "",
        street: "",
        number: "",
        zipCode: ""
    };
    constructor(public storage: Storage, public registerService: RegisterService,
        public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation,
        public nativeGeocoder: NativeGeocoder, public camera: Camera, public formBuilder: FormBuilder) {
        this.userNew = {};
        this.form = this.formBuilder.group({
            colony: ['', Validators.required],
            street: ['', Validators.required],
            number: ['', Validators.required],
            zipCode: ['', Validators.required],
        });

        this.colony = this.form.controls['colony'];
        this.street = this.form.controls['street'];
        this.number = this.form.controls['number'];
        this.zipCode = this.form.controls['zipCode'];
    }

    ionViewDidEnter() {
        this.storage.ready().then(() => {
            this.storage.get('userId').then((valor) => {
                this.userId = valor;
                this.getUser(valor);
            });
        });
    }

    getUser(userId) {
        this.registerService.getUser(userId).subscribe(res => {
            this.userNew = res.users;

            console.log(this.userNew);
            if (this.userNew.address != null) {
                console.log(this.userNew.address);
                this.usuario.number = this.userNew.address.number;
                this.usuario.street = this.userNew.address.street;
                this.usuario.suburb = this.userNew.address.suburb;
                this.usuario.zipCode = this.userNew.address.zipCode;
                console.log(this.usuario);
                //this.colony=this.usuario.suburb;
                //this.street=this.usuario.street;
                //this.number=this.usuario.number;
                //this.zipCode=this.usuario.zipCode;
                //this.colony = this.userNew.address.suburb;
                //this.street = this.userNew.address.street;
                //this.number = this.userNew.address.number;
                //this.zipCode = this.userNew.address.zipCode;
                //this.latitude = this.userNew.address.latitude;
                //this.longitude = this.userNew.address.longitude;
            }

        }, err => {
            console.log(err);
        });
    }

    updateUser() {
        if (!this.form.valid) {
            this.isSent = true;
            return;
        }
        this.userNew.address = {
            suburb: this.usuario.suburb,
            street: this.usuario.street,
            number: this.usuario.number,
            zipCode: this.usuario.zipCode,
            latitude: this.latitude,
            longitude: this.longitude,
        }
        console.log(this.userId);
        console.log(this.userNew);
        this.registerService.updateUser(this.userId, this.userNew).subscribe(
            data => {
                console.log(data);
            },
            err => {
                console.log(err);
            });
        //this.navCtrl.push(DetailPage,this.form.value)
    }

    verifyAddress() {
        this.verify = true;
        this.geolocation.getCurrentPosition().then((resp) => {
            var lat1, lng1, lat2, lng2;
            //this.nativeGeocoder.forwardGeocode('Ampliación Paraíso Higuera 35 63038')
            let address = this.colony.value + " " + this.street.value + " " + this.number.value + " " + this.zipCode.value + " ";
            this.nativeGeocoder.forwardGeocode(address)
                .then((coordinates: NativeGeocoderForwardResult) => {
                    lat1 = resp.coords.latitude.toFixed(2);
                    lng1 = resp.coords.longitude.toFixed(2);
                    lat2 = parseFloat(coordinates.latitude).toFixed(2);
                    lng2 = parseFloat(coordinates.longitude).toFixed(2);
                    if (lat1 == lat2 && lng1 == lng2) {
                        this.verified = true;
                        this.latitude = resp.coords.latitude;
                        this.longitude = resp.coords.longitude;
                        console.log('Verificado: '+this.latitude+' '+this.longitude);
                    } else {
                        this.verified = false;
                    }
                    console.log('Analizado');
                })
                .catch((error: any) => {
                    console.log(error);
                });
        }).catch((error) => {
            console.log('Error getting location', error);
        });
    }

    getPicture() {
        this.camera.getPicture({
            quality: 50,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.FILE_URI,
            targetWidth: 800,
            targetHeight: 800,
            sourceType: 1
        }).then((imageData) => {
            this.imageData = imageData;
        }, (err) => {
            console.log(err);
        });
    }

    public send() {
        this.isSent = true;
        if (!this.form.valid || !this.verified || !this.imageData) {
            return;
        }
        this.updateUser();

        //this.navCtrl.push(DetailPage, this.form.value)
    }
}