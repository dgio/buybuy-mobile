import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from '../../services/product-service';
import { RegisterService } from '../../services/register-service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-product-new',
  templateUrl: 'product-new.html',
})
export class ProductNew {
  public name: any = "";
  public description: any = "";
  public price: any = "";
  public product = {_id:"", name: "", description: "", price : "", company: "", category: "japonesa"};
  public userId;
  public userName;
  public isSent: boolean = false;
  public form: FormGroup;

  constructor(public storage: Storage, public navCtrl: NavController,
    public navParams: NavParams, public formBuilder: FormBuilder,
    public productService: ProductService, public registerService: RegisterService) {

    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      price: ['', Validators.required],
    });

    this.name = this.form.controls['name'];
    this.description = this.form.controls['description'];
    this.price = this.form.controls['price'];

    this.storage.ready().then(() => {
      this.storage.get('userId').then((valor) => {
        this.userId = valor;
        console.log("GET: "+this.userId);
      });
    });
  }

  ionViewDidEnter() {
    this.registerService.getUser(this.userId).subscribe(res => {
      this.userName = res.users;
    }, err => {
      console.log(err);
    });

    this.productService.getProduct(this.navParams.data._id).subscribe(res => {
      this.product = res.products;
      console.log("Producto");
      console.log(this.product);
    }, err => {
      console.log(err);
    });
  }

  public send() {
    this.isSent = true;
    if (!this.form.valid) {
      return;
    }
    
    
    if (this.navParams.data.insert === true) {
      let product = { name: this.product.name, description: this.product.description, price: this.product.price, company: "Happy Sumo", category: "japonesa" };
      this.productService.createProduct(product).subscribe(
        data => {
          console.log(data);
          this.navCtrl.pop();
        },
        err => {
          console.log(err);
          this.navCtrl.pop();
        });
    }
    else {
      console.log(this.product);
      this.product = { _id: this.product._id, name: this.product.name, description: this.product.description, price: this.product.price, company: "Happy Sumo", category: "japonesa" };
      this.productService.updateProduct(this.product._id, this.product).subscribe(
        data => {
          console.log(data);
          this.navCtrl.pop();
        },
        err => {
          console.log(err);
          this.navCtrl.pop();
        });
    }
    //this.navCtrl.push(DetailPage,this.form.value)
  }

}
