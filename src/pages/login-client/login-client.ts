import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms'
import { Storage } from '@ionic/storage';
import { RegisterService } from '../../services/register-service';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';

import { RegisterClient } from '../register-client/register-client';
import { Menu } from '../menu/menu';

@Component({
  selector: 'page-login-client',
  templateUrl: 'login-client.html',
})
export class LoginClient {
  FB_APP_ID: number = 852361741568166;//212699022541750;
  //Reverse Client ID: com.googleusercontent.apps.709717175992-5pkuscmut8ukptkhu5e5o6bsvipa8060
  /*
  ionic plugin add cordova-plugin-googleplus --variable REVERSED_CLIENT_ID=com.googleusercontent.apps.709717175992-5pkuscmut8ukptkhu5e5o6bsvipa8060
  npm install --save @ionic-native/google-plus

  ionic plugin add cordova-plugin-facebook4 --variable APP_ID="852361741568166" --variable APP_NAME="BuyBuy"
  */
  public loginFB: boolean;
  public loginGP: boolean;

  userName: any = "";
  password: any = "";
  userName2: any = "";
  password2: any = "";
  user: any;
  unregistered = false;
  loginFail: boolean = false;

  public result: any = {};
  public facebook: any = {};
  public google: any = {};
  public isSent: boolean = false;
  public form: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, public registerService: RegisterService,
    public formBuilder: FormBuilder, public loadingController: LoadingController,
    private fb: Facebook, public gp: GooglePlus) {

    this.user = {};

    this.form = this.formBuilder.group({
      userName: ['jess', Validators.required],
      password: ['jess123', Validators.required],
    });

    this.userName = this.form.controls['userName'];
    this.password = this.form.controls['password'];

    this.fb.browserInit(this.FB_APP_ID, "v2.8");
    this.loginFB = false;
  }

  doFacebookLogin() {
    this.loginFB = true;
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
        this.fb.api('/me?fields=name,email', [])
          .then(fbData => {
            fbData.picture = "https://graph.facebook.com/" + fbData.id + "/picture?type=large";
            this.result = fbData;
            this.registerService.getUserFB(fbData.id).subscribe(res => {
              this.user = res.users;
              console.log(this.user);
              if (!this.user) {
                let usuario = {
                  _id: fbData.id,
                  usertype: 0,
                  authType: 1,
                  name: fbData.name,
                  email: fbData.email,
                  picture: fbData.picture,
                  "user.id": fbData.id
                };

                this.registerService.createUser(usuario).subscribe(data => {
                  this.user = data.users;
                }, err => {
                  console.log("Error", err);
                });
              }
              this.storage.set('userId', this.user._id);
            }, err => {
              console.log(err);
            });
            this.openMenu(1);
          }).catch(err => {
            this.result = err;
          });
      })
      .catch(err => {
        this.facebook = err;
        console.log('Error logging into Facebook', err)
      });
  }

  doFacebookLogout() {
    this.fb.logout();
  }

  doGoogleLogin() {
    this.loginGP = true;
    this.gp.login({})
      .then(dataGP => {
        this.google = dataGP;
        this.registerService.getUserFB(dataGP.userId).subscribe(res => {
          this.user = res.users;
          console.log(this.user);
          if (!this.user) {
            let usuario = {
              usertype: 0,
              authType: 2,
              name: dataGP.displayName,
              email: dataGP.email,
              picture: dataGP.imageUrl,
              "user.id": dataGP.userId
            };
            console.log(usuario);

            this.registerService.createUser(usuario).subscribe(data => {
              this.user = data.users;
            }, err => {
              console.log("Error", err);
            });
          }
          this.storage.set('userId', this.user._id);
        }, err => {
          console.log(err);
        });
        this.openMenu(2);
      })
      .catch(err => {
        console.error(err);
        this.google = err;
      });
  }

  doGoogleLogout() {
    this.gp.logout();
  }

  getUserId() {
    this.registerService.getUserId(this.userName2).subscribe(res => {
      this.user = res.users;
      this.unregistered = false;

      if (this.user == undefined) {
        this.unregistered = true;
        return;
      }

      this.storage.set('userId', this.user._id);

      console.log(this.user._id);
    }, err => {
      console.log(err);
    });
  }

  openRegister() {
    this.navCtrl.push(RegisterClient);
  }

  openMenu(type) {
    //this.navCtrl.setRoot(Menu);
    if (!this.loginFB && !this.loginGP) {
      this.getUserId();
    }
    if (this.unregistered) {
      return;
    }
    if (type == 0 && !this.loginFB && !this.loginGP) {
      if (!this.form.valid) {
        this.isSent = true;
        return;
      }
    }
    switch (this.user.authType) {
      case 0:
        this.loginFail = false;
        if (this.user.user.password != this.password2 || this.user.user.name != this.userName2) {
          this.loginFail = true;
          return;
        }
        this.navCtrl.push(Menu, true);
        break;
      case 1:
        this.loginFB = false;
        this.navCtrl.push(Menu, true);
        break;
      case 2:
        this.navCtrl.push(Menu, true);
        break;
    }
  }

}