import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    LatLng,
    CameraPosition,
    MarkerOptions,
    Marker
} from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Camera } from "@ionic-native/camera";
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';

import { UserType } from '../components/userType';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginClient } from '../pages/login-client/login-client';
import { LoginCompany } from '../pages/login-company/login-company';
import { RegisterClient } from '../pages/register-client/register-client';
import { RegisterCompany } from '../pages/register-company/register-company';
import { Profile } from '../pages/profile/profile';
import { Menu } from '../pages/menu/menu';
import { Categories } from '../pages/categories/categories';
import { Companies } from '../pages/companies/companies';
import { Products } from '../pages/products/products';
import { ProductDetail } from '../pages/product-detail/product-detail';
import { Address } from '../pages/address/address'
import { IonicStorageModule } from '@ionic/storage';
import { ProductsCompany } from "../pages/products-company/products-company";
import { ProductNew } from "../pages/product-new/product-new";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    UserType,
    LoginClient,LoginCompany,
    RegisterClient,RegisterCompany,
    Profile,Menu,
    Categories,Companies,
    Products,ProductDetail,
    ProductsCompany,ProductNew,
    Address
  ],
  imports: [
    BrowserModule, HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    UserType,
    LoginClient,LoginClient,
    RegisterClient,RegisterCompany,
    Profile,Menu,
    Categories,Companies,
    Products,ProductDetail,
    ProductsCompany,ProductNew,
    Address
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    NativeGeocoder,
    Camera,
    Facebook,
    GooglePlus,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
