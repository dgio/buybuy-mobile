import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductDetail } from '../product-detail/product-detail';
import { ProductService } from '../../services/product-service';
@Component({
	selector: 'page-products',
	templateUrl: 'products.html',
})
export class Products {
	company: string;
	allProducts:Array<{}>;
	constructor(public productService:ProductService,public navCtrl: NavController, public navParams: NavParams) {
		this.company = navParams.data;
	}
	ionViewDidEnter(){
		this.productService.getProductCompany(this.company).subscribe(res=>{
			this.allProducts=res.products;
		},(err)=>{
			console.log(err.products);
		});
		console.log(this.allProducts);
	}
	openProductDetails(productName:string){
		this.navCtrl.push(ProductDetail, {product: productName, user: true});
	}
	ionViewDidLoad() {
		console.log('ionViewDidLoad Products');
	}

}
