import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RegisterCompany } from '../register-company/register-company';
import { Menu } from '../menu/menu';
import { Storage } from '@ionic/storage';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms'
import { RegisterService } from '../../services/register-service';

@Component({
	selector: 'page-login-company',
	templateUrl: 'login-company.html',
})
export class LoginCompany {

	userName: any = "";
	password: any = "";
	userName2: any = "";
	password2: any = "";
	user: any;
	loginFail: boolean = false;
	public isSent: boolean = false;
	public form: FormGroup;

	constructor(public storage: Storage, public navCtrl: NavController,
		public navParams: NavParams, public formBuilder: FormBuilder,
		public registerService: RegisterService) {

		this.user = {};

		this.form = this.formBuilder.group({
			userName: ['happysumo', Validators.required],
			password: ['sushi123', Validators.required],
		});

		this.userName = this.form.controls['userName'];
		this.password = this.form.controls['password'];

		this.registerService.getUserId("happysumo").subscribe(
			res => {
				this.user = res.users;
				this.storage.set('userId', this.user._id);
				console.log(this.user._id);
			}, err => {
				console.log(err);
			});
	}

	openRegister() {
		this.navCtrl.push(RegisterCompany);
	}

	openMenu() {
		if (!this.form.valid) {
			this.isSent = true;
			return;
		}
		this.loginFail = false;
		if (this.user.user.password != this.password2 || this.user.user.name != this.userName2) {
			this.loginFail = true;
			return;
		}
		this.navCtrl.push(Menu, false);
	}
}