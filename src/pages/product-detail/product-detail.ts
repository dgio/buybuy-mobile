import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductService } from '../../services/product-service';
@Component({
	selector: 'page-product-detail',
	templateUrl: 'product-detail.html',
})
export class ProductDetail {
	productID: string;
	productInfo={
		name:"",
		category:"",
		price:"",
		description:"",
		company:""
	};
	
	constructor(public productService:ProductService,public navCtrl: NavController, public navParams: NavParams) {
		this.productID=navParams.data.product;
	}
	ionViewDidEnter(){
		this.productService.getProduct(this.productID).subscribe((res)=>{
			this.productInfo.name=res.products.name;
			//this.productInfo.category=res.products.category;
			this.productInfo.price=res.products.price;
			this.productInfo.description=res.products.description;

			console.log("ok");
		},(err)=>{
			console.log(err.products);
		});
		console.log(this.productInfo)
	}
	ionViewDidLoad() {
		console.log('ionViewDidLoad ProductDetail');
	}

}
