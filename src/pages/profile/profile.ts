import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Camera } from "@ionic-native/camera";
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms'
import { RegisterService } from '../../services/register-service';
import { Storage } from '@ionic/storage';


@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
})
export class Profile {
    public imageData: string;

    public fullname: any = "";
    public email: any = "";
    public password: any = "";
    public password2 = "";

    public isSent: boolean = false;
    public form: FormGroup;
    public picture;
    public userId;
    public userNew: any;

    public social:boolean=false;;

    constructor(public storage: Storage, public registerService: RegisterService, public navCtrl: NavController, public navParams: NavParams, public camera: Camera, public formBuilder: FormBuilder) {
        this.userNew = {};
        this.form = this.formBuilder.group({
            fullname: ['', Validators.required],
            email: ['', [Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"), Validators.required]],
            password: ['', Validators.required],
        });

        this.fullname = this.form.controls['fullname'];
        this.email = this.form.controls['email'];
        this.password = this.form.controls['password'];

    }

    ionViewDidEnter() {
        this.storage.ready().then(() => {
            this.storage.get('userId').then((valor) => {
                this.userId = valor;
                this.getUser(valor);
            });
        });
        this.imageData = this.userNew.picture;
    }

    getUser(userId) {
        this.registerService.getUser(userId).subscribe(res => {
            this.userNew = res.users;
            if(this.userNew.user.id){
                this.social=true;
            }
            console.log(this.userNew);
            this.password2 = this.userNew.user.password;
            this.picture = this.userNew.picture;
        }, err => {
            console.log(err);
        });
    }

    updateUser() {
        if (!this.form.valid) {
            this.isSent = true;
            return;
        }
        this.userNew.user = { password: this.password2, name: this.userNew.user.name }
        console.log(this.userId);
        console.log(this.userNew);
        this.registerService.updateUser(this.userId, this.userNew).subscribe(
            data => {
                console.log(data);
            },
            err => {
                console.log(err);
            });
        //this.navCtrl.push(DetailPage,this.form.value)
    }

    getPicture(sourceType) {
        this.camera.getPicture({
            quality: 50,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: true,
            destinationType: this.camera.DestinationType.FILE_URI,
            targetWidth: 800,
            targetHeight: 800,
            sourceType: sourceType
        }).then((imageData) => {
            this.imageData = imageData;
            this.userNew.picture = this.imageData;
        }, (err) => {
            console.log(err);
        });
    }
}
