import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Companies } from '../companies/companies';
@Component({
	selector: 'page-categories',
	templateUrl: 'categories.html',
})
export class Categories {
	allCategories:Array<{title:string,key:string}>;
	
	constructor(public navCtrl: NavController, public navParams: NavParams) {
		this.allCategories=[
			{title:"Comida Japonesa",key:"japonesa"},
			{title:"Comida Italiana",key:"italiana"},
			{title:"Lonchería",key:"loncheria"},
			{title:"Comida mexicana",key:"mexicana"},
			{title:"Comida china",key:"china"},
			{title:"Snacks",key:"snacks"},
			{title:"Postres",key:"postres"}
		]
	}
	openCompanies(companyName:string){
		this.navCtrl.push(Companies,companyName);
	}
	ionViewDidLoad() {
		console.log('ionViewDidLoad Categories');
	}

}
