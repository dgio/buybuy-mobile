import { Injectable } from "@angular/core";
import { Http, RequestOptions, Headers } from "@angular/http";
import 'rxjs/Rx';

@Injectable()
export class RegisterService{
    http: any;
    baseURL: string;

    constructor(http: Http){
        this.http = http;
        this.baseURL = 'http://37.139.28.66/BuyBuy/users/';
    }

    createUser(user: any){
        let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
        
        return this.http.post(this.baseURL, user, options)
            .map(res => res.json())
            .catch(err => console.log(err));
    }

    getUserId(userName){
        return this.http.get(this.baseURL + "username/" + userName).map(
            res => res.json());
    }

    getUser(id) {
		return this.http.get(this.baseURL + id)
			.map(res => res.json());
	}

    getUserFB(idFb){
        return this.http.get(this.baseURL+"/fb/"+idFb)
        .map(res => res.json());
    }

    getUserCategory(category) {
		return this.http.get(this.baseURL+"/company/" + category)
			.map(res => res.json());
	}

    updateUser(id, user) {
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers })
		return this.http.put(this.baseURL + id, user, options)
			.map(res => res.json())
			.catch(err => console.log(err));
	}
}