import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Camera } from "@ionic-native/camera";
import { ProductNew } from "../product-new/product-new";
import { ProductService } from '../../services/product-service';
import { RegisterService } from '../../services/register-service';
import { Storage } from '@ionic/storage';
import { ProductDetail } from '../product-detail/product-detail';

@Component({
    selector: 'page-products-company',
    templateUrl: 'products-company.html',
})
export class ProductsCompany {
    public products: Array<{}>;
    public userId;
    public user;

    constructor(public storage: Storage, public navCtrl: NavController,
        public navParams: NavParams, public camera: Camera, public productService: ProductService,
        public registerService: RegisterService) {
        this.storage.ready().then(() => {
            this.storage.get('userId').then((valor) => {
                this.userId = valor;
                console.log(this.userId);
            });
        });
    }

    getProducts() {
        this.productService.getProductCompany("Happy Sumo").subscribe( res=>{
            this.products=res.products;
		},(err)=>{
			console.log(err.products);
		});
        console.log(this.products);
    }

    ionViewDidEnter() {
        this.getProducts();
    }

    public newProduct() {
        this.navCtrl.push(ProductNew, {insert:true});
    }

    public updateProduct(product) {
        this.navCtrl.push(ProductNew, product);
    }

    public deleteProduct(product) {
        this.productService.deleteProduct(product._id).subscribe(
            data => {
                console.log(data);
            },
            err => {
                console.log(err);
            });
    }

    openProductDetails(productName:string){
		this.navCtrl.push(ProductDetail, {product: productName, user: false});
	}
}
