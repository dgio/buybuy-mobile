import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Nav, Platform } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { Storage } from '@ionic/storage';
import { Profile } from '../profile/profile';
import { Categories } from '../categories/categories';
import { Address } from "../address/address";
import { ProductsCompany } from "../products-company/products-company";
import { HomePage } from '../home/home';
import { RegisterService } from '../../services/register-service';

@Component({
	selector: 'menu',
	templateUrl: 'menu.html'
})
export class Menu {
	@ViewChild(Nav) nav: Nav;

	rootPage: any = Categories;

	pages: Array<{ title: string, component: any }>;
	userId;
	userNew: any;

	constructor(public storage: Storage, public navCtrl: NavController,
	public navParams: NavParams, private fb: Facebook, public gp: GooglePlus, public registerService: RegisterService) {
		var a: boolean = this.navParams.data;
		if (a) {
			this.rootPage = Categories;
			this.pages = [
				{ title: 'Categorías', component: Categories },
				{ title: 'Perfil', component: Profile },
				{ title: 'Dirección', component: Address }
			];
		} else {
			this.rootPage = ProductsCompany;
			this.pages = [
				{ title: 'Productos', component: ProductsCompany },
				{ title: 'Perfil', component: Profile },
				{ title: 'Dirección', component: Address }
			];
		}
	}

	ionViewDidEnter() {
		this.storage.ready().then(() => {
			this.storage.get('userId').then((valor) => {
				this.userId = valor;
				this.getUser(valor);
			});
		});
	}

	getUser(userId) {
		this.registerService.getUser(userId).subscribe(res => {
			this.userNew = res.users;
			console.log(this.userNew);
		}, err => {
			console.log(err);
		});
	}

	openPage(p) {
		this.nav.setRoot(p.component);
	}

	logoutFB() {
		var nav = this.navCtrl;
		this.fb.logout()
			.then(function (response) {
				this.storage.set('userId', "");
				//nav.remove(nav.last().index);
				nav.setRoot(HomePage);
			}, function (error) {
				console.log(error);
			});
	}

	logout() {
		try {
			this.fb.logout();
			this.gp.logout()
			this.storage.set('userId', "");
			this.navCtrl.setRoot(HomePage);
		} catch (e) {
			console.log(e);
		}
	}
}