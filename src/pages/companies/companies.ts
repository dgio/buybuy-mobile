import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Products } from '../products/products';
import { RegisterService } from '../../services/register-service';
@Component({
	selector: 'page-companies',
	templateUrl: 'companies.html',
})
export class Companies {
	category:string;
	allCompanies:Array<{}>;
	constructor(public registerService:RegisterService,public navCtrl: NavController, public navParams: NavParams) {
		this.category=navParams.data;
		
	}
	ionViewDidEnter(){
		this.registerService.getUserCategory(this.category).subscribe((res)=>{
			this.allCompanies=res.users;
		},(err)=>{
			console.log(err.users);
		});
		console.log(this.allCompanies);
	}
	openProducts(company:string){
		this.navCtrl.push(Products,company);
	}
	ionViewDidLoad() {
		console.log('ionViewDidLoad Companies');
	}

}
