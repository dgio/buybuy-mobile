import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms'
import { RegisterService } from '../../services/register-service';
import { Camera } from "@ionic-native/camera";

@Component({
  selector: 'page-register-client',
  templateUrl: 'register-client.html',
})
export class RegisterClient {

  newUser: any;
  name: any = "";
  password: any = "";
  email: any = "";
  user: any = "";
  dataUser: any;
  public	imageData:string;
  picture= "https://api.adorable.io/avatars/285/abott@adorable.io";

  userForm: FormGroup;
  public isSent: boolean = false;

  constructor(public navCtrl: NavController, public camera: Camera, public navParams: NavParams, public registerService: RegisterService, public fb: FormBuilder) {
    this.newUser = {};
    this.dataUser = {};
    this.userForm = this.fb.group({
      'name': ['', Validators.compose([Validators.required])],
      'email': ['', Validators.compose([Validators.required])],
      'password': ['', Validators.compose([Validators.required])],
      'user': ['', Validators.compose([Validators.required])]
    });

    this.name = this.userForm.controls['name'];
    this.email = this.userForm.controls['email'];
    this.password = this.userForm.controls['password'];
    this.user = this.userForm.controls['user'];
  }

  insertUser() {
    if (!this.userForm.valid) {
      this.isSent = true;
      return;
    }

    //this.user.photo = this.photo + this.user.email + ".png";
    this.newUser.authType = 0;
    this.newUser.usertype = 0;
    this.newUser.user = this.dataUser;
    this.registerService.createUser(this.newUser).subscribe(
      data => {
        console.log(data);
        this.navCtrl.pop();
      },
      err => {
        console.log(err);
        this.navCtrl.pop();
      });
  }

  getPicture(sourceType) {
    this.camera.getPicture({
      quality: 50,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      targetWidth: 800,
      targetHeight: 800,
      sourceType: sourceType
    }).then((imageData) => {
      this.imageData = imageData;
    }, (err) => {
      console.log(err);
    });
  }

}
