import { Injectable } from "@angular/core";
import { Http, RequestOptions, Headers } from "@angular/http";
import 'rxjs/Rx';

@Injectable()
export class ProductService{
    http: any;
    baseURL: string;

    constructor(http: Http){
        this.http = http;
        this.baseURL = 'http://37.139.28.66/BuyBuy/products/';
    }

    createProduct(user: any){
        let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
        
        return this.http.post(this.baseURL, user, options)
            .map(res => res.json())
            .catch(err => console.log(err));
    }

    getProducts() {
		return this.http.get(this.baseURL)
			.map(res => res.json());
	}

    getProduct(id) {
		return this.http.get(this.baseURL+"product/" + id)
			.map(res => res.json());
	}

    getProductCompany(company) {
		return this.http.get(this.baseURL + company)
			.map(res => res.json());
	}
    deleteProduct(id) {
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers })
		return this.http.delete(this.baseURL + id, options)
			.catch(err => console.log(err));
	}

    updateProduct(id, product) {
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers })
		return this.http.put(this.baseURL + id, product, options)
			.map(res => res.json())
			.catch(err => console.log(err));
	}
}